FROM registry.gitlab.com/av1o/base-images/alpine:3 AS base

FROM base AS build-base

USER 0
RUN apk add --no-cache curl
USER 1001


# prepare kubectl
FROM build-base AS kubectl
ARG KUBECTL_VERSION
ARG KUBECTL_CHECKSUM

RUN echo "Downloading Kubernetes $KUBECTL_VERSION@$KUBECTL_CHECKSUM"

WORKDIR /home/somebody

ARG SOURCE=https://storage.googleapis.com/kubernetes-release/release/v$KUBECTL_VERSION/bin/linux/amd64/kubectl
ARG TARGET=./kubectl
RUN curl -fLSs "$SOURCE" -o "$TARGET"
RUN sha256sum "$TARGET"
RUN echo "$KUBECTL_CHECKSUM *$TARGET" | sha256sum -c -

# prepare helm
FROM build-base AS helm
ARG HELM_VERSION
ARG HELM_CHECKSUM

RUN echo "Downloading Helm $HELM_VERSION@$HELM_CHECKSUM"

WORKDIR /home/somebody

ARG SOURCE=https://get.helm.sh/helm-v$HELM_VERSION-linux-amd64.tar.gz
ARG TARGET=./helm.tar.gz
RUN curl -fLSs "$SOURCE" -o "$TARGET"
RUN sha256sum "$TARGET"
RUN echo "$HELM_CHECKSUM *$TARGET" | sha256sum -c -
RUN mkdir -p ./helm
RUN tar -xvf "$TARGET" -C ./helm

FROM build-base AS debugger

RUN curl -fLSs https://dcas-artifacts.s3.us-west-002.backblazeb2.com/k8s-kubectl-dbg/kubectl-dbg -o /tmp/kubectl-dbg

# create final dir structure
FROM base

USER 0
RUN apk add --no-cache ca-certificates git

COPY --from=kubectl /home/somebody/kubectl /usr/bin/
COPY --from=helm /home/somebody/helm/linux-amd64/helm /usr/bin/
COPY --from=debugger /tmp/kubectl-dbg /usr/bin/

RUN chown 1001:0 /usr/bin/kubectl-dbg /usr/bin/helm /usr/bin/kubectl
RUN chmod +x /usr/bin/kubectl-dbg /usr/bin/helm /usr/bin/kubectl

USER somebody

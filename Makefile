.PHONY:
build-1.18:
	docker build \
		--build-arg=KUBECTL_VERSION=1.18.0 \
		--build-arg=KUBECTL_CHECKSUM=bb16739fcad964c197752200ff89d89aad7b118cb1de5725dc53fe924c40e3f7 \
		--build-arg=HELM_VERSION=3.2.4 \
		--build-arg=HELM_CHECKSUM=8eb56cbb7d0da6b73cd8884c6607982d0be8087027b8ded01d6b2759a72e34b1 \
		.

.PHONY:
build-1.23:
	docker build \
		--build-arg=KUBECTL_VERSION=1.23.4 \
		--build-arg=KUBECTL_CHECKSUM=3f0398d4c8a5ff633e09abd0764ed3b9091fafbe3044970108794b02731c72d6 \
		--build-arg=HELM_VERSION=3.8.0 \
		--build-arg=HELM_CHECKSUM=8408c91e846c5b9ba15eb6b1a5a79fc22dd4d33ac6ea63388e5698d1b2320c8b \
		.
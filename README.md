# Helm install image

The purpose of this repository is to build a minimal image with `helm` and `kubectl` installed
for the purpose of GitLab's cluster integration.

Tags:
* (deprecated) `v3.2.4-1.18.0`, `v3.2-1.18`, `v3-1.18` - Helm `v3.2.4`, Kubectl `v1.18.0`
* `v3.8.0-1.23.4`, `v3.8-1.23`, `v3-1.23` - Helm `v3.8.0`, Kubectl `v1.23.4`
* `v3.10.0-1.25.2`, `v3.10-1.25`, `v3-1.25` - Helm `v3.10.0`, Kubectl `v1.25.2`
